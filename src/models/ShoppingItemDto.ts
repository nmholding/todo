export default interface ShoppingItemDto {
  id: string;
  name: string;
  done: boolean;
  listId: string;
}