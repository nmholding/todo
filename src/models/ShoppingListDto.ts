import ShoppingItemDto from '@/models/ShoppingItemDto';

export default interface ShoppingListDto {
  id: string;
  name: string;
  items: ShoppingItemDto[];
}
