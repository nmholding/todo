import Vue from 'vue';
import Vuetify, { VSnackbar } from 'vuetify/lib';
import VuetifyToast from 'vuetify-toast-snackbar';
import App from '@/App.vue';
import '@/assets/global.css';
import vuetify from '@/plugins/vuetify';
import i18n from '@/i18n';
import store from '@/store';
import router from '@/router';

// newest version of vuetify brings some changes into snackbars so VuetifyToast plugin need some correction https://github.com/eolant/vuetify-toast-snackbar/issues/42
Vue.use(Vuetify, {
  components: {
    VSnackbar,
  },
});

const vueObj = new Vuetify();
export default vueObj;
Vue.use(VuetifyToast, {
  $vuetify: vueObj.framework,
  x: 'right',
  y: 'bottom',
  color: 'info',
  icon: 'mdi-information-outline',
  iconColor: '',
  timeout: 3000,
  dismissable: true,
  multiLine: false,
  vertical: false,
  queueable: false,
  showClose: true,
  closeText: '',
  closeIcon: 'mdi-close',
  closeColor: '',
  slot: [],
  shorts: {
    custom: {
      color: 'purple',
    },
  },
  property: '$toast',
});

export const app = new Vue({
  vuetify,
  store,
  router,
  i18n,
  render: h => h(App),
}).$mount('#app');
