import {
  Action,
  getModule,
  Module,
  Mutation,
  VuexModule,
} from 'vuex-module-decorators';
import store from '@/store';
import ShoppingItemDto from '@/models/ShoppingItemDto';
import { randomString } from '@/utils/randomString';
import ShoppingListDto from '@/models/ShoppingListDto';

const listId1 = randomString(10);

const testItems = [
  {
    id: randomString(10),
    name: 'test',
    done: false,
    listId: listId1,
  },
];

const testLists = [
  {
    id: listId1,
    name: 'list 1',
    items: testItems,
  },
  {
    id: randomString(10),
    name: 'list 2',
    items: [],
  },
];

@Module({
  store,
  name: 'shopping',
  dynamic: true,
  namespaced: true,
  preserveState: localStorage.getItem('vuex') !== null,
})
class ShoppingModule extends VuexModule {
  private lists: ShoppingListDto[] = [];
  private allItemsMap: Map<
    string,
    [ShoppingItemDto, ShoppingListDto]
  > = new Map();
  private searchText = '';
  private onlyDone = false;

  public get allItemsView() {
    return this.shoppingListsView.reduce((acc, list) => {
      acc.push(...list.items);
      return acc;
    }, new Array<ShoppingItemDto>());
  }

  public get searchValue() {
    return this.searchText;
  }

  public get onlyDoneItemsValue() {
    return Boolean(this.onlyDone);
  }

  @Mutation
  private addNewList(): void {
    const listId = randomString(10);
    this.lists.unshift({
      id: listId,
      name: '',
      items: [
        {
          id: randomString(10),
          name: '',
          done: false,
          listId,
        },
      ],
    });
  }

  @Mutation
  private removeExistingList(payload: ShoppingListDto): void {
    const originLength = this.lists.length;
    this.lists = this.lists.filter(list => list.id !== payload.id);
    if (originLength === this.lists.length) {
      // TODO show error
    }
  }

  @Mutation
  private addNewItem(list: ShoppingListDto): void {
    const foundList = this.lists.find(list => list.id === list.id);

    if (foundList) {
      foundList.items.push({
        id: randomString(10),
        name: '',
        done: false,
        listId: foundList.id,
      });
    } else {
      // TODO show error
    }
  }

  @Mutation
  private removeExistingItem(payload: ShoppingItemDto): void {
    const foundItem = this.allItemsMap.get(payload.id);
    if (!foundItem) {
      // TODO show error
      return;
    }
    const originLength = foundItem[1].items.length;
    foundItem[1].items = foundItem[1].items.filter(
      item => item.id !== payload.id
    );
    if (originLength === foundItem[1].items.length) {
      // TODO show error
    }
  }

  @Mutation
  private findItem(payload: ShoppingItemDto): ShoppingItemDto | null {
    const foundItem = this.allItemsMap.get(payload.id);
    if (foundItem) {
      return foundItem[0];
    }
    // TODO show error
    return null;
  }

  @Mutation
  private setSearchText(text: string): void {
    this.searchText = text;
  }

  @Mutation
  public updateItem(payload: ShoppingItemDto): void {
    const foundItem = this.allItemsMap.get(payload.id);
    if (!foundItem) {
      // TODO show error
      return;
    }

    foundItem[0].name = payload.name;
    foundItem[0].done = payload.done;
  }

  @Mutation
  public filterOnlyDoneItems(value: boolean): void {
    this.onlyDone = value;
  }

  public get shoppingListsView() {
    if (!this.searchText && !this.onlyDone) {
      return this.lists;
    }

    return this.lists.map(list => ({
      ...list,
      items: list.items.filter(
        item =>
          item.name.includes(this.searchText) &&
          (!this.onlyDone || Boolean(item.done) === true)
      ),
    }));
  }

  @Mutation
  private updateItemsMap() {
    this.allItemsMap = new Map();
    this.lists.forEach(list => {
      list.items.forEach(item => this.allItemsMap.set(item.id, [item, list]));
    });
  }

  @Action({ rawError: true })
  public initModule() {
    if (this.lists.length === 0) {
      this.addNewList();
    }
    this.updateItemsMap();
  }

  @Action({ rawError: true })
  public addList(): void {
    this.addNewList();
    this.updateItemsMap();
  }

  @Action({ rawError: true })
  public removeList(list: ShoppingListDto): void {
    this.removeExistingList(list);
    this.updateItemsMap();
  }

  @Action({ rawError: true })
  public addItem(list: ShoppingListDto): void {
    this.addNewItem(list);
    this.updateItemsMap();
  }

  @Action({ rawError: true })
  public removeItem(payload: ShoppingItemDto): void {
    this.removeExistingItem(payload);
    this.updateItemsMap();
  }

  @Action({ rawError: true })
  public search(text: string) {
    this.setSearchText(text);
  }
}

export default getModule(ShoppingModule);