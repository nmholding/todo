import Vue from 'vue';
import VueRouter from 'vue-router';
import HomePage from '@/pages/HomePage.vue';
import SearchPage from '@/pages/SearchPage.vue';
import Paths from '@/constants/Paths';
import PageNames from '@/constants/PageNames';
import AppLayout from '@/layouts/AppLayout.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '',
    name: PageNames.APP_LAYOUT,
    component: AppLayout,
    redirect: Paths.HOME,
    children: [
      {
        path: Paths.HOME,
        name: PageNames.HOME,
        component: HomePage,
      },
      {
        path: Paths.SEARCH,
        name: PageNames.SEARCH,
        component: SearchPage,
      },
    ],
  },
];

const router = new VueRouter({
  routes,
});

export default router;