import Vue from 'vue';
import Vuetify, { VIcon, VSnackbar } from 'vuetify/lib';
import colors from 'vuetify/lib/util/colors';

import { Ripple } from 'vuetify/lib/directives';
import { DISPLAY_BREAKPOINT_XS } from '../../config/config';

Vue.use(Vuetify, {
  components: {
    VSnackbar,
    VIcon,
  },
});

export default new Vuetify({
  treeShake: false,
  directives: {
    Ripple, // to show action from a user - click on a button etc.
  },
  icons: {
    iconfont: 'mdi', // default - only for display purposes
  },
  breakpoint: {
    thresholds: {
      xs: DISPLAY_BREAKPOINT_XS,
    },
  },
  theme: {
    themes: {
      light: {
        systemPrimary: colors.red.lighten1,
        buttonPrimary: colors.green.darken1,
        buttonCancel: colors.red.darken1,
      },
    },
  },
});