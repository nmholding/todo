export default class PageNames {
  public static APP_LAYOUT = 'App layout';
  public static HOME = 'Home';
  public static SEARCH = 'Search';
}