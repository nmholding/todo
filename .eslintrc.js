module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/essential',
    'eslint:recommended',
    'plugin:vue-i18n/recommended',
    '@vue/typescript/recommended',
    '@vue/prettier',
    '@vue/prettier/@typescript-eslint',
  ],
  parserOptions: {
    ecmaVersion: 2020,
  },
  settings: {
    'vue-i18n': {
      localeDir: './locales/*.json',
    },
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-param-reassign': ['error', { props: false }],
    'arrow-parens': ['error', 'as-needed'],
    'import/prefer-default-export': 0,
    'max-classes-per-file': ['error', 10],
    // the following rule is taken care of by prettier
    '@typescript-eslint/indent': 0,
    prefixWithI: 0,
    'vue-i18n/no-raw-text': [
      'error',
      {
        ignoreNodes: ['md-icon', 'v-icon'],
      },
    ],
  },
  globals: {
    __static: 'readonly',
  },
  overrides: [
    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
        '**/tests/unit/**/*.spec.{j,t}s?(x)',
      ],
      env: {
        jest: true,
      },
    },
  ],
};
