// see https://vuetifyjs.com/en/features/breakpoints/ and vuetify.ts
export const DISPLAY_BREAKPOINT_XS: number | undefined = process.env
    .VUE_APP_DISPLAY_BREAKPOINT_XS
    ? parseInt(process.env.VUE_APP_DISPLAY_BREAKPOINT_XS, 10)
    : 600;